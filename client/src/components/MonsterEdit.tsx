import React from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Monster from "../models/monster";

export interface Props {
  monster: Monster;
}

class MonsterEdit extends React.Component<Props, object> {

  monster: Monster = new Monster();

  handleSubmit(event: React.FormEvent) {
    event.preventDefault();

    // fetch('/api/form-submit-url', {
    //   method: 'POST',
    //   body: JSON.stringify(this.monster),
    // });
  }

  render() {
    return (
      <div>
        <h2>Monster</h2>
        <Form onSubmit={(e: any) => { this.handleSubmit(e) }}>
          <Form.Group controlId="monsterName">
            <Form.Label>Name</Form.Label>
            <Form.Control placeholder="Aboleth" onChange={(e: any) => { this.monster.name = e.target.value as string }} />
            <Form.Text className="text-muted">Enter the monster's name</Form.Text>
          </Form.Group>

          <Form.Group controlId="Pronoun">
            <Form.Label>Name</Form.Label>
            <Form.Control placeholder="it" />
            {/* <Form.Text className="text-muted">Enter the monster's name</Form.Text> */}
          </Form.Group>

          <Button variant="primary" type="submit">Save</Button>
        </Form>
      </div>
    );
  }
};

export default MonsterEdit;
