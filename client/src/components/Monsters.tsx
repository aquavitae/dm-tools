import React, { ReactNode } from "react";
import MonsterEdit from "./MonsterEdit";
import Monster from "../models/monster";

export interface Props {
  data: string;
}

class Monsters extends React.Component<Props, object> {
  [data: string]: ReactNode;

  componentDidMount() {
    fetch(`https://jsonplaceholder.typicode.com/users`)
      .then(response => response.json())
      .then(data =>
        this.setState({
          data: 'got data',
        })
      )
      .catch(error => this.setState({ data: error }));
  }

  render() {
    return (
      <div>
        <h2>Monsters</h2>
        <p>{this.data}</p>
        <MonsterEdit monster={new Monster()} />
      </div>
    );
  }
};

export default Monsters;
